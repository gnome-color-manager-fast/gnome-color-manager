# French translation for gnome-color-manager.
# Copyright (C) 2012 gnome-color-manager's COPYRIGHT HOLDER
# This file is distributed under the same license as the gnome-color-manager package.
#
# Bruno Brouard <annoa.b@gmail.com>, 2012.
msgid ""
msgstr ""
"Project-Id-Version: gnome-color-manager master\n"
"POT-Creation-Date: 2012-05-19 09:11+0000\n"
"PO-Revision-Date: 2012-09-23 19:46+0200\n"
"Last-Translator: Bruno Brouard <annoa.b@gmail.com>\n"
"Language-Team: French <gnomefr@traduc.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"Language: fr\n"
"X-Generator: Lokalize 1.4\n"

#: C/legal.xml:3(p/link)
msgid "Creative Commons Attribution-ShareAlike 3.0 Unported License"
msgstr ""
"Creative Commons Paternité-Partage des Conditions Initiales à l'Identique "
"3.0 Unported"

#: C/legal.xml:3(license/p)
msgid "This work is licensed under a <_:link-1/>."
msgstr "Cette œuvre est distribuée sous licence <_:link-1/>."

#. Put one translator per line, in the form NAME <EMAIL>, YEAR1, YEAR2
msgctxt "_"
msgid "translator-credits"
msgstr "Bruno Brouard <annoa.b@gmail.com>, 2012"

#: C/color-import-linux.page:9(info/desc)
msgid "How to import an existing ICC profile using a Linux system."
msgstr "Comment importer un profil ICC existant en utilisant un système Linux."

#: C/color-import-linux.page:11(credit/name)
#: C/color-import-windows.page:11(credit/name)
#: C/color-import-osx.page:11(credit/name)
msgid "Richard Hughes"
msgstr "Richard Hughes"

#: C/color-import-linux.page:17(page/title)
msgid "Installing an ICC profile on Linux"
msgstr "Installation d'un profil ICC sur Linux"

#: C/color-import-linux.page:18(page/p)
msgid ""
"If you have <cmd>gnome-color-manager</cmd> or <cmd>colord-kde</cmd> "
"installed then just double click the <file>.icc</file> profile and click "
"<gui>Import</gui>. You can then assign the new profile to an existing device "
"using the <guiseq><gui>System Settings</gui><gui>Color</gui></guiseq> panel."
msgstr ""
"Si vous avez installé <cmd>gnome-color-manager</cmd> ou <cmd>colord-kde</"
"cmd>, faites tout simplement un double-clic sur le fichier de profil <file>."
"icc</file> et cliquez sur <gui>Importer</gui>. Vous pouvez alors attribuer "
"le nouveau profil à un périphérique existant en utilisant le panneau "
"<guiseq><gui>Paramètres système</gui><gui>Couleur</gui></guiseq>."

#: C/color-import-linux.page:25(page/p)
msgid ""
"Profile calibration curves are automatically loaded at login, or can be "
"loaded for all users at startup if the <gui>Set for all users</gui> button "
"is clicked."
msgstr ""
"Les courbes de calibration du profil sont chargées automatiquement lorsque "
"vous vous connectez ou elles peuvent être chargées pour tous les "
"utilisateurs si le bouton <gui>Définir pour tous les utilisateurs</gui> est "
"cliqué."

#: C/color-import-windows.page:9(info/desc)
msgid "How to import an existing ICC profile using a Windows system."
msgstr ""
"Comment importer un profil ICC existant en utilisant un système Windows."

#: C/color-import-windows.page:17(page/title)
msgid "Installing an ICC profile on Microsoft Windows"
msgstr "Installation d'un profil ICC sur Microsoft Windows"

#: C/color-import-windows.page:18(page/p)
msgid ""
"The method for assigning the profile to a device and also using the embedded "
"calibration curves is different for each version of Microsoft Windows."
msgstr ""
"La méthode pour attribuer le profil au périphérique et pour utiliser les "
"courbes de calibration intégrées, est différente pour chaque version de "
"Microsoft Windows."

#: C/color-import-windows.page:24(section/title)
msgid "Windows XP"
msgstr "Windows XP"

#: C/color-import-windows.page:25(section/p)
msgid ""
"Right click on the profile in Windows Explorer and click <gui>Install "
"profile</gui>. This copies the profile to the correct directory "
"automatically."
msgstr ""
"Faites un clic droit sur le profil dans Windows Explorer et cliquez sur "
"<gui>Installer le profil</gui>. Cela effectue une copie du profil dans le "
"répertoire correct automatiquement."

#: C/color-import-windows.page:29(section/p)
msgid ""
"Then open <guiseq><gui>Control Center</gui><gui>Color</gui></guiseq> and add "
"the profile to the device."
msgstr ""
"Ouvrez alors le <guiseq><gui>centre de contrôle</gui><gui>Couleur</gui></"
"guiseq> et ajoutez le profil au périphérique."

#: C/color-import-windows.page:34(note/p)
msgid ""
"If you are replacing an existing profile in Windows XP, the above shortcut "
"does not work. The profiles must be manually copied to <file>C:\\Windows"
"\\system32\\spool\\drivers\\color</file> for the original profile to be "
"replaced."
msgstr ""
"Si vous remplacez un profil existant dans Windows XP, le raccourci ci-dessus "
"ne fonctionne pas. Les profils doivent être copiés manuellement dans <file>C:"
"\\Windows\\system32\\spool\\drivers\\color</file> pour que le profil "
"d'origine soit remplacé."

#: C/color-import-windows.page:42(section/p)
msgid ""
"Windows XP requires a program to be run at startup to copy the profile "
"calibration curves into the video card. This can be done using <app>Adobe "
"Gamma</app>, <app>LUT Loader</app> or by using the free <app href=\"https://"
"www.microsoft.com/download/en/details.aspx?displaylang=en&amp;id=12714\"> "
"Microsoft Color Control Panel Applet</app>. Using the latter adds a new "
"<gui>Color</gui> item to the control panel and allows the calibration curves "
"from default profile to be easily set at each startup."
msgstr ""
"Windows XP a besoin d'un programme qui doit être lancé au démarrage pour "
"copier les courbes de calibration du profil dans la carte vidéo. Cela peut "
"être réalisé en utilisant <app>Adobe Gamma</app>, <app>LUT Loader</app> ou "
"en utilisant le logiciel libre <app href=\"https://www.microsoft.com/"
"download/en/details.aspx?displaylang=en&amp;id=12714\">Microsoft Color "
"Control Panel Applet</app>. En utilisant ce dernier, un élément "
"<gui>Couleur</gui> est ajouté au panneau de contrôle ce qui permet de "
"définir facilement les courbes de calibration à partir du profil par défaut "
"à chaque démarrage."

#: C/color-import-windows.page:55(section/title)
msgid "Windows Vista"
msgstr "Windows Vista"

#: C/color-import-windows.page:56(section/p)
msgid ""
"Microsoft Windows Vista mistakenly removes calibration curves from the video "
"LUT after logon, after suspend, and when the UAC screen appears. This means "
"you may have to manually reload the ICC profile calibration curves. If you "
"are using profiles with embedded calibration curves you have to be very "
"careful the calibration state has not been cleared."
msgstr ""
"Microsoft Windows Vista supprime par erreur les courbes de calibration de la "
"LUT vidéo après la connexion, après la mise en veille et lorsque l'écran UAC "
"apparaît. Cela signifie que vous devez recharger manuellement les courbes de "
"calibration du profil ICC. Si vous utilisez des profils contenant des "
"courbes de calibration intégrées, il est plus prudent de vérifier que l'état "
"de la calibration n'a pas été supprimé."

#: C/color-import-windows.page:68(section/title)
msgid "Windows 7"
msgstr "Windows 7"

#: C/color-import-windows.page:69(section/p)
msgid ""
"Windows 7 supports a similar scheme to Linux, in that profiles can be "
"installed system-wide or specific to the user. They are however stored in "
"the same place. Right click on the profile in Windows Explorer and click "
"<gui>Install profile</gui> or copy the .icc profile to <file>C:\\Windows"
"\\system32\\spool\\drivers\\color</file>."
msgstr ""
"Windows 7 prend en charge un schéma similaire à Linux par le fait que les "
"profils peuvent être installés pour tout le système ou spécifiquement à "
"l'utilisateur. Ils sont cependant enregistrés au même endroit. Faites un "
"clic droit sur le profil dans l'Explorateur Windows et cliquez sur "
"<gui>Installer le profil</gui> ou copiez le profil .icc dans <file>C:"
"\\Windows\\system32\\spool\\drivers\\color</file>."

#: C/color-import-windows.page:76(section/p)
msgid ""
"Open <guiseq><gui>Control Center</gui><gui>Color Management</gui></guiseq> "
"and then add the profile to the system by clicking the <gui>Add</gui> "
"button. Click on the <gui>Advanced</gui> tab and look for <gui>Display "
"Calibration</gui>. Calibration curve loading is enabled by the <gui>Use "
"Windows display calibration</gui> checkbox but it is not sensitive. This can "
"be enabled by clicking on <gui>Change system defaults</gui> and then "
"returning to the <gui>Advanced</gui> tab and then clicking the checkbox."
msgstr ""
"Ouvrez le <guiseq><gui>Centre de contrôle</gui><gui>Gestion de la couleur</"
"gui></guiseq> puis ajoutez le profil au système en cliquant sur "
"<gui>Ajouter</gui>. Cliquez sur l'onglet <gui>Avancé</gui> et recherchez "
"<gui>Calibration de l'affichage</gui>. Le chargement des courbes de "
"calibration est activé par la case à cocher <gui>Utiliser la calibration "
"d'affichage Windows</gui> mais ce n'est pas sensible. Cela peut être activé "
"en cliquant sur <gui>Modifier les paramètres par défaut du système</gui> "
"puis retournez dans l'onglet <gui>Avancé</gui> et cochez la case."

#: C/color-import-windows.page:87(section/p)
msgid ""
"Close the dialog and click <gui>Reload current calibrations</gui> to set the "
"gamma ramps. The profile calibration curves should now be set for every boot."
msgstr ""
"Fermez la boîte de dialogue et cliquez sur <gui>Recharger la calibrations "
"actuelles</gui> pour définir les rampes gamma. Les courbes de calibration du "
"profil devraient maintenant être définies à chaque démarrage."

#: C/color-import-osx.page:9(info/desc)
msgid "How to import an existing ICC profile using an OS X system."
msgstr "Comment importer un profil ICC existant en utilisant un système OS X."

#: C/color-import-osx.page:17(page/title)
msgid "Installing an ICC profile on Apple OS X"
msgstr "Installation d'un profil ICC sur Apple OS X"

#: C/color-import-osx.page:18(page/p)
msgid ""
"Apple OS X supports a similar scheme to Linux, in that profiles can be "
"installed system-wide or specific to the user. System wide profiles are "
"stored in <file>/Library/ColorSync/Profiles</file> and user-specific "
"profiles are stored in <file>~/Library/ColorSync/Profiles</file>."
msgstr ""
"Apple OS X prend en charge un schéma similaire à Linux par le fait que les "
"profils peuvent être installés pour tout le système ou spécifiquement à "
"l'utilisateur. Les profils pour tout le système sont enregistrés dans <file>/"
"Library/ColorSync/Profiles</file> et les profils spécifiques à l'utilisateur "
"dans <file>~/Library/ColorSync/Profiles</file>."

#: C/color-import-osx.page:24(page/p)
msgid ""
"Use the <guiseq><gui>System Preferences</gui><gui>Displays</gui><gui>Color</"
"gui></guiseq> tool to import the file and assign the profile to the correct "
"device."
msgstr ""
"Utilisez l'outil <guiseq><gui>Préférences système</gui><gui>Moniteurs</"
"gui><gui>Couleur</gui></guiseq> pour importer le fichier et attribuer le "
"profil au périphérique correct."
